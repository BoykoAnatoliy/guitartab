class Admin::SessionsController < AdminsController
  skip_before_action :require_admin, only: [:new, :create]

  def create
    if params[:email] == 'admin@mail.ru' && params[:password] == 'qazwsx123'
      session[:admin] = true
      redirect_to admin_users_path
    else
      render :new
    end
  end

  def destroy
    session[:admin] = false
    redirect_to admin_root_path
  end
end
