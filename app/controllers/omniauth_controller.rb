class OmniauthController < ApplicationController
  skip_before_action :require_user
  before_action :current_resources

  def create
    if @user
      @account[user: @user]
    else
      @user = @account.create_user(vk_params)
    end
    @account.save
    session[:current_user_id] = @user.token
    redirect_to root_path
  end

  private

  def current_resources
    @account = Account.find_or_create_by(uid: auth_hash[:uid],
                                         provider: auth_hash[:provider],
                                         email: auth_hash[:info][:email])
    @user = User.find_by(email: auth_hash[:info][:email])
  end

  def vk_params
    {
      email: auth_hash[:info][:email],
      password_digest: auth_hash[:uid]
    }
  end

  def fb_params
    {
      email: auth_hash[:info][:email],
      remote_avatar_url: auth_hash[:info][:image],
      password: password,
      password_confirmation: password
    }
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
