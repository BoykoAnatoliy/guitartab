class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :category
      t.string :code

      t.timestamps null: false
    end
  end
end
